package com.book.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Response;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import com.book.service.representation.OrderRequest;

public final class BookServiceClient {

    private BookServiceClient() {
    } 

    public static void main(String args[]) throws Exception {
    	
    	List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
        
        /*****************************************************************************************
         * GET METHOD invoke
         *****************************************************************************************/
        System.out.println("GET METHOD .........................................................");
        WebClient getClient = WebClient.create("http://localhost:8080", providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
        //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        getClient = getClient.accept("application/json").type("application/json").path("/bookservice/book/3");
        
        //The following lines are to show how to log messages without the CXF interceptors
        String getRequestURI = getClient.getCurrentURI().toString();
        System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
        String getRequestHeaders = getClient.getHeaders().toString();
        System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);
        
        //to see as raw XML/json
        String response = getClient.get(String.class);
        System.out.println("GET METHOD Response: ...." + response);
        
        /*****************************************************************************************
         * GET METHOD invoke
         *****************************************************************************************/
        System.out.println("GET METHOD .........................................................");
        WebClient getAllClient = WebClient.create("http://localhost:8080", providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(getAllClient).getOutInterceptors().add(new LoggingOutInterceptor());
        //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(getAllClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        getAllClient = getAllClient.accept("application/json").type("application/json").path("/bookservice/books/James Fox/");
        
        //The following lines are to show how to log messages without the CXF interceptors
        String getAllRequestURI = getAllClient.getCurrentURI().toString();
        System.out.println("Client GET METHOD Request URI:  " + getAllRequestURI);
        String getAllRequestHeaders = getAllClient.getHeaders().toString();
        System.out.println("Client GET METHOD Request Headers:  " + getAllRequestHeaders);
        
        //to see as raw XML/json
        String response2 = getAllClient.get(String.class);
        System.out.println("GET METHOD Response: ...." + response2);
        
        /*****************************************************************************************
         * PUT METHOD invoke
        *****************************************************************************************/
        System.out.println("PUT METHOD .........................................................");
        WebClient putClient = WebClient.create("http://localhost:8080", providers);
        WebClient.getConfig(putClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(putClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        putClient = putClient.accept("application/xml").type("application/json").path("/orderservice/order");
     	
        String putRequestURI = putClient.getCurrentURI().toString();
        System.out.println("Client PUT METHOD Request URI:  " + putRequestURI);
        String putRequestHeaders = putClient.getHeaders().toString();
        System.out.println("Client PUT METHOD Request Headers:  " + putRequestHeaders);
        
        OrderRequest orderRequest = new OrderRequest();
    	orderRequest.setIsbn(3);
    	
    	orderRequest.setShippingAddress("123 N Howard Chicago, IL 60660");
    	orderRequest.setPayment(55.50);
        
     	String responsePut =  putClient.put(orderRequest, String.class);
        System.out.println("POST MEDTHOD Response ........." + responsePut);
        
        /*****************************************************************************************
         * GET METHOD invoke
         *****************************************************************************************/
        System.out.println("GET METHOD .........................................................");
        getClient = WebClient.create("http://localhost:8080", providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
      //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        getClient = getClient.accept("application/json").type("application/json").path("/orderservice/status/1");
        
        //The following lines are to show how to log messages without the CXF interceptors
        getRequestURI = getClient.getCurrentURI().toString();
        System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
        getRequestHeaders = getClient.getHeaders().toString();
        System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);
        
        //to see as raw XML/json
        response = getClient.get(String.class);
        System.out.println("GET METHOD Response: ...." + response);
        
        
        
        /*****************************************************************************************
         * DELETE METHOD invoke
        *****************************************************************************************/
        System.out.println("DELETE METHOD .........................................................");
        WebClient deleteClient = WebClient.create("http://localhost:8080", providers);
        WebClient.getConfig(deleteClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(deleteClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to application/json get in json format
        deleteClient = deleteClient.accept("application/xml").type("application/json").path("/orderservice/order/1");
     	
        String deleteRequestURI = deleteClient.getCurrentURI().toString();
        System.out.println("Client DELETE METHOD Request URI:  " + deleteRequestURI);
        String deleteRequestHeaders = deleteClient.getHeaders().toString();
        System.out.println("Client DELETE METHOD Request Headers:  " + deleteRequestHeaders);
        
        deleteClient.delete();
        System.out.println("DELETE MEDTHOD Response ......... OK");    	
    	
    	System.exit(0);

    }

}