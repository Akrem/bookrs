package com.book.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.book.business.*;
import com.book.data.BookStoreRepository;
import com.book.service.representation.*;

@Path("/bookservice/")
public class BookResource implements BookService{
	
	private BookStoreRepository bookStoreRepository;
		
	public BookResource(){
		bookStoreRepository = BookStoreRepository.Instance();
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/books/{author}")
	public Book[] searchBooks(@PathParam("author") String author) {
		return bookStoreRepository.searchBooks(author);
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/book/{isbn}")
	public Book searchBook(@PathParam("isbn") long isbn) {
		return bookStoreRepository.searchBook(isbn);
	}

}
