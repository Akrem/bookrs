package com.book.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderRequest {
	private long _isbn;
	private String _shippingAddress;
	private double _payment;
	
	public long getIsbn() {
		return _isbn;
	}
	public void setIsbn(long isbn) {
		_isbn = isbn;
	}
	
	public String getShippingAddress() {
		return _shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		_shippingAddress = shippingAddress;
	}
	
	public double getPayment() {
		return _payment;
	}
	public void setPayment(double payment) {
		_payment = payment;
	}
}
