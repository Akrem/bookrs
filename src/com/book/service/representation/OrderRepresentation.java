package com.book.service.representation;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "OrderId")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderRepresentation {
	
	private int _orderId;
	
	public void setOrderId(int orderId){
		_orderId=orderId;
	}
	
	public long getOrderId(){
		return _orderId;
	}
}
