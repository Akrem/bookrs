package com.book.service;

import javax.jws.WebService;

import com.book.business.*;
import com.book.service.representation.*;

@WebService
public interface BookService {
	public Book[] searchBooks(String author);

	public Book searchBook(long isbn);
}