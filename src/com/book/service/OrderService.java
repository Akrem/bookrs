package com.book.service;

import javax.jws.WebService;
import javax.ws.rs.core.Response;

import com.book.service.representation.OrderRepresentation;
import com.book.service.representation.OrderRequest;

@WebService
public interface OrderService {
	
	public String orderStatus(int orderId);
	
	public Response cancelOrder(int orderId);
	
	public OrderRepresentation buyBook(OrderRequest orderRequest);
}
