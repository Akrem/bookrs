package com.book.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.book.data.BookStoreRepository;
import com.book.service.representation.OrderRepresentation;
import com.book.service.representation.OrderRequest;

@Path("/orderservice/")
public class OrderResource implements OrderService {

	private BookStoreRepository bookStoreRepository;
	
	public OrderResource(){
		bookStoreRepository = BookStoreRepository.Instance();
	}
	
	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/order")
	public OrderRepresentation buyBook(OrderRequest orderRequest) {
		return bookStoreRepository.buyBook(orderRequest.getIsbn(), orderRequest.getShippingAddress(), orderRequest.getPayment());
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/status/{orderId}")
	public String orderStatus(@PathParam("orderId") int orderId) {
		return bookStoreRepository.orderStatus(orderId);
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/order/{orderid}")
	public Response cancelOrder(@PathParam("orderId") int orderId) {
		boolean success = bookStoreRepository.cancelOrder(orderId);
		if (success) {
			return Response.status(Status.OK).build();
		}
		return null;
		
	}
}