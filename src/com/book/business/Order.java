package com.book.business;

public class Order {
	private int _orderId;
	private long _isbn;
	private String _shipping;
	private double _payment;
	private OrderStatus _orderStatus;
	
	public Order(int orderId, long isbn, String shipping, double payment) {
		_orderId = orderId;
		_isbn = isbn;
		_shipping = shipping;
		_payment = payment;
	}
	
	public int getOrderId() {
		return _orderId;
	}
	public long getIsbn() {
		return _isbn;
	}
	
	public String getShipping(){
		return _shipping;
	}
	
	public double getPayment(){
		return _payment;
	}
	public OrderStatus getOrderStatus(){
		return _orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		_orderStatus = orderStatus;
	}
}
